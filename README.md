## CSCF Staff Meetings

We have a full CSCF staff meeting every month or two (termly at minimum), based on having sufficient
agenda items to warrant getting together. Meetings are generally aimed at
presentations likely relevant to the group as a whole, including topics of
general technical interest, lessons learned from attending courses or other
Professional Development, or topics that management would like to discuss with
the group. 

To access the minutes, please visit the [wiki](https://git.uwaterloo.ca/cscf/minutes/wikis)

These are minutes of past meetings. Meetings further in the past are documented
[elswhere](https://cs.uwaterloo.ca/twiki/view/CFPrivate/CscfStaffMeetings).
